package com.china.myapp.url;

/**
 * Created by ShellJor on 2017/7/10 0010.
 * at 14:56
 */

public class UrlConfig {

    public static final String BASE_URL = "";
    //验证手机号码的正则。
    public final static String MATCH_PHONE = "^1[34578]{1}\\d{9}$";

    //验证密码是数字字母组合正则
    public static final String MATCH_PWD = ".*[a-zA-Z].*[0-9]|.*[0-9].*[a-zA-Z]";

}
